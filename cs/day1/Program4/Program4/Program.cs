﻿using System;

namespace Program4
{
    class MainClass
    {
        public static bool isPrime(int n)
        {
            if (n == 1) return false;
            for(int i = 2; i < n; i++)
            {
                if (n % i == 0) return false;
            }
            return true;
        }
        public static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] a = new int[n];
            int ans = 0;
            string s = Console.ReadLine();
            string[] arr_s = s.Split(' ');
            for(int i = 0; i < n; i++)
            {
                int x = int.Parse(arr_s[i]);
                if (isPrime(x) == true) ans++; 
            }
            Console.Write(ans);
        }
    }
}
