﻿using System;

namespace Program5
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Student s = new Student("Aset", 1, 3);
            Console.WriteLine(s.getId());
            Console.WriteLine(s.getName());
            s.incofYear();
            Console.WriteLine(s.year);

        }
    }
}

//https://informatics.mccme.ru/
/* 
if (condition1) {
operations1
}
else if (condition2) {
operations2
}
.....
else {
operations
}
*/