﻿using System;
namespace Program5
{
    public class Student
    {
        string name;
        int id;
        public int year;
        //static int university;
        public Student(string name, int id, int year) {
            this.name = name;
            this.id = id;
            this.year = year;
        }
        public string getName()
        {
            return this.name;
        }
        public int getId()
        {
            return this.id;
        }
        public void incofYear()
        {
            this.year++;
        }
    }
}
