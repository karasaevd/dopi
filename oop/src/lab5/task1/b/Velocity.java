package lab5.task1.b;

public interface Velocity extends Moveable {
    public double velocity(String animal);
}
