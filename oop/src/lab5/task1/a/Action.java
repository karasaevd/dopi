package lab5.task1.a;

public interface Action {
    public void move();
    public void sound();
}
