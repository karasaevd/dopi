package lab5.task3;

import java.util.Collection;

public interface MyCollection { //неотсортированный, с дупликатами
    public void push(int x);
    public void delete(int x);
    public void deleteNth(int n);
    public boolean contains(int x);
    public boolean isEmpty();
    public int size();
}
