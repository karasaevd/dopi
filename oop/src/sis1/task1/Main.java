package sis1.task1;

public class Main {
    public static void main(String[] args) {
        Temperature t = new Temperature(32, 'C');
        System.out.println(t.getTemperatureInF());
        t.setScale('F');
        System.out.println(t.getTemperatureInF() + " " + t.getScale());
    }
}
