package sis2.task1;

public class Series extends Circuit {
    double resistance;
    double potenDiff;
    public Series(Circuit a, Circuit b) {
        resistance = a.getResistance() + b.getResistance();
        potenDiff = a.getPotenDiff() + b.getPotenDiff();
    }
    public double getResistance() {
        return resistance;
    }

    public double getPotenDiff() {
        return potenDiff;
    }

    public void setPotenDiff(double V) {
        potenDiff = V;
    }
}
