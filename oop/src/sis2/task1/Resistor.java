package sis2.task1;

public class Resistor extends Circuit {
    double resistance;
    double potenDiff;
    public Resistor(double resistance) {
        this.resistance = resistance;
        this.potenDiff = 5;
    }
    public double getResistance() {
        return resistance;
    }

    public double getPotenDiff() {
        return potenDiff;
    }

    public void setPotenDiff(double V) {
        this.potenDiff = V;
    }
}
