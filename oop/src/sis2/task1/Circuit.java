package sis2.task1;

public abstract class Circuit {
    public Circuit() {

    }
    public abstract double getResistance();
    public abstract double getPotenDiff();
    public abstract void setPotenDiff(double V);
    public double getPower() {
        return getPotenDiff() * getPotenDiff() / getResistance(); // P = V * V / R;
    }
    public double getCurrent() {
        return getPotenDiff() / getResistance(); // I = V / R;
    }
}