package sis2.task1;

public class Parallel extends Circuit {
    double resistance;
    double potenDiff;
    public Parallel(Circuit a, Circuit b) {
        resistance = (a.getResistance() * b.getResistance()) / (a.getResistance() + b.getResistance());
        potenDiff = a.getPotenDiff();
    }
    public double getResistance() {
        return resistance;
    }

    public double getPotenDiff() {
        return potenDiff;
    }

    public void setPotenDiff(double V) {
        potenDiff = V;
    }
}
