#include <iostream>
#include <map>

using namespace std;

int main() {
	int n, m;
	cin >> n >> m;
	int a[n], b[m];
	map<int, int> mp;
	for(int i = 0; i < n; i++) {
		cin >> a[i];
		mp[a[i]]++;
	}
	for(int i = 0; i < m; i++) {
		cin >> b[i];
		for(int j = 0; j < mp[b[i]]; j++) {
			cout << b[i] << " ";
		}
		mp[b[i]] = 0;
	}
	for(map<int, int>::iterator it = mp.begin(); it != mp.end(); it++) {
		for(int i = 0; i < (*it).second; i++)
		cout << (*it).first << ' ';
	}
}