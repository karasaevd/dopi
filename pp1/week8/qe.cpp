#include <iostream>

using namespace std;

int main() {
	int n, k, m;
	cin >> n >> k >> m;
	string s[n];
	m -= 1;
	for(int i = 0; i < n; i++) cin >> s[i];
	for(int i = max(0, m - k); i < min(m + k, n); i++) {
		cout << s[i] << endl;
	}
}