#include <iostream>

using namespace std;

int main() {
	int n;
	cin >> n;
	int a[n];
	int inf = -10000;
	for(int i = 0; i < n; i++) cin >> a[i];
	for(int i = 0; i < n - 1; i++) {
		int temp = a[i];
		if (temp == a[i + 1]) {
			a[i + 1] = inf;
			i++;
		}
	}
	for(int i = 0; i < n; i++) {
		if (a[i] != inf) {
			cout << a[i] << ' ';
		}
	}	
}