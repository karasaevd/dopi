#include <iostream>

using namespace std;

int main() {
	int n;
	cin >> n;
	int a[n];
	for(int i = 0; i < n; i++) cin >> a[i];
	int m;
	cin >> m;
	int b[m];
	for(int i = 0; i < m; i++) cin >> b[i];
	int c[n + m];
	int i = 0, j = 0;
	while(i + j < n + m) {
		if (j == m) {
			c[i + j] = a[i];
			i++;
		}
		else if (i == n) {
			c[i + j] = b[j];
			j++;
		}
		else if (a[i] > b[j]) {
			c[i + j] = b[j];
			j++;
		}
		else {
			c[i + j] = a[i];
			i++;
		}
	}
	for(int i = 0; i < n + m; i++) {
		cout << c[i] << ' ';
	}
}

// 5
// 4 5 6 7 10
// 	       i
// 5
// 2 4 6 7 8
// 	       j

// 10 
// 2 4 4 5 6 6 7 7 8 10
