#include <iostream>

using namespace std;

int ans(int n) {
	if (n == 0) {
		return 0;
	}
	return (n % 2) + 10 * ans(n / 2);
}

int main() {
	int n;
	cin >> n;
	cout << ans(n);
}

// 5 101
// 1 + 10 * f(5 / 2) == 1
// 0 + 10 * f(2 / 2) == 0
// 1 + f(0) = 1