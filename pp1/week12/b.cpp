#include <iostream>

using namespace std;

long long f(long n) {
	if (n == 0 || n == 1) return 1;
	return n * f(n - 1);
}

int main() {
	cout << f(14);
}