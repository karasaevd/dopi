#include <iostream>

using namespace std;

int a[1001];
int n;
bool ans(int x, int i) {
	if (a[i] == x) {
		return true;
	}
	if (i == n - 1) {
		return false;
	}
	return ans(x, i + 1);
}

int main() {
	cin >> n;
	for(int i = 0; i < n; i++) cin >> a[i];
	int x;
	cin >> x;
	if (ans(x, 0)) cout << "YES";
	else cout << "NO";
}